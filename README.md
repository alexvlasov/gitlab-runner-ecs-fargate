# gitlab-runner-ec-fargate

A gitlab runner that can run on ECS and Fargate. Supports taking tokens as ENV rather than having a config.toml directory